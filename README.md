# nodeEmail

#### 介绍
node发邮件

1. 安装 nodemailer

   `cnpm i nodemailer -S`

2.  安装定时任务包

    `cnpm i node-schedule -S`

3.  创建连接

    ```javascript
    console.log("创建连接");
    let con = nodemailer.createTransport({
      host: "smtp.qq.com", //邮箱服务的主机，如smtp.qq.com
      port: 465, //对应的端口号
      //开启安全连接
      secure: true, // secure:true for port 465, secure:false for port 587
      secureConnection: true, // use SSL
    
      //用户信息
      auth: {
        user: "542356155@qq.com",
        pass: "dnpsoekyylsnbbee", // QQ邮箱需要使用授权码
      },
    });
    ```

4.  设置收件人配置

    ```javascript
    //设置收件人信息
    let mailOptions = {
      from: '"Mrlih" <542356155@qq.com>"', //谁发的 格式 ’这里是发件人名字" <xx@qq.com>‘
      to: "542356155@qq.com", //发给谁
      subject: "ccccssss", //主题是什么
      text: "啊实打实大苏打自行车行政村", //文本内容
      html: "<html><h1>world</h1></html>", //html模板
    
      // //附件信息
      // attachments: [
      //   {
      //     filename: "",
      //     path: "",
      //   },
      // ],
    };
    ```

5.  发送

    ```javascript
    // 使用先前创建的传输器的 sendMail 方法传递消息对象
    con.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(error);
        return console.log(error);
      } else {
        console.log(`Message: ${info.messageId}`);
        console.log(`sent: ${info.response}`);
      }
    });
    ```

    

